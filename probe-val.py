#!/usr/bin/python
from ace2f5 import *


'''
Validation:
    1. if Rserver port from validation file matches Rserver port from ACE config file F5 config should be correct.
    2. if Rserver port from validation file is '0'
        a. and all VIP ports match the Probe default port all Pool Members can be set to using the Probe Default Port
        b. if VIP port does not match Probe default port then manual investigation is required, likely need cloned Monitor will destination port set
'''


def getportnum(port):
    if port == 'http' or port == 'www':
        return '80'
    elif port == 'https':
        return '443'
    elif port == 'any':
        return '0'
    else:
        return port


def getportname(port):
    if port == '80':
        return 'http'
    elif port == '443':
        return 'https'
    elif port == '0':
        return 'any'
    else:
        return port


def getVIPfromServerfarm(serverfarm):
    policy_lst = []
    sticky_lst = []
    pmclass_lst = []
    vip_lst = []
    for sticky in Sticky.all_objs.values():
        if hasattr(sticky, 'serverfarm') and sticky.serverfarm == serverfarm:
            sticky_lst.append(sticky.name)
    for policy in PolicyLB.all_objs.values():
        if policy.sticky and policy.sticky_serverfarm in sticky_lst:
            policy_lst.append(policy.name)
        elif (hasattr(policy, 'serverfarm') and policy.serverfarm == serverfarm):
            policy_lst.append(policy.name)
    for pmclass in PolicyMapClass.all_objs.values():
        if hasattr(pmclass, 'lb_policy') and pmclass.lb_policy in policy_lst:
            pmclass_lst.append(pmclass.name)
    for classmap in ClassMap.all_objs.values():
        if hasattr(classmap, 'vips') and classmap.name in pmclass_lst:
            for vip in classmap.vips:
                yield vip['port']


class probe_detailed:
    real_re = re.compile(r'[\[,\]]')
    def __init__(self, config):
        self.config = config
        *_, self.name = self.config.splitlines()[0].strip().split()
        _, _, self.default_port, *_ = self.config.splitlines()[3].strip().split()
        self.serverfarms = []
        self.vipportmismatch = False
        print('\n\nProbe: %s\tPort: %s' % (self.name, self.default_port))
        for line in self.config.splitlines():
            if line.startswith('   serverfarm  :'):
                self.serverfarms.append(line.split()[-1])
                print('\tServerfarm: %s\tVIPS: ' % (self.serverfarms[-1]), end = '')
                for vip in getVIPfromServerfarm(self.serverfarms[-1]):
                    print('%s ' % (getportnum(vip)), end = '')
                if getportname(self.default_port) not in getVIPfromServerfarm(self.serverfarms[-1]):
                    print('\t## Probe default port and VIP port mismatch', end='')
                    self.vipportmismatch = True
                print()
            elif line.startswith('     real      :'):
                *_, real = line.split()
                rserver, port = real.split('[')
                port = port[:-1]
                print('\t\tRServer: %s\t%s ' % (rserver, port), end='')
                for RServer, Port in ServerfarmHost.all_objs[self.serverfarms[-1]].rservers:
                    if rserver == RServer:
                        print('\t'+Port, end='')
                        if port != '0' and port == getportnum(Port):
                            print()
                        elif port == '0' and self.vipportmismatch:
                            print('\t## Port mismatch, check required')
                        else:
                            print()


class probe_validation:
    all_objs = {}
    probe_cmd = re.compile(r'^ probe       :')
    def __init__(self, file):
        self.conf_blocks = []
        self.error_blocks = []
# Clears block var ready for configuration split
        block = ''
        file = open(file, 'rt')
# Take each line from file and split into blocks bases on the spacing at the start of each line.
        for line in file:
# Regex match for line starting with [a-z], i.e. not a 'space', 'tab' or '#'
            if self.probe_cmd.match(line):
# As new root configuration block found adds 'block' to list of blocks, clears the 'block' var and add first line
                if block != '':
                    self.conf_blocks.append(block)
                block = line
# If line doesn't start with [a-z] line is part of block so adds the line to the 'block' var
            elif line.startswith(' '):
                block = block + line
# Adds final block to list of blocks
        self.conf_blocks.append(block)
        file.close()
        for block in self.conf_blocks:
            temp = probe_detailed(block)
            probe_validation.all_objs[temp.name] = temp


if __name__=='__main__':
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="ace_file", type="str",
                      default="", help="ACE Configuration file")
    parser.add_option("-p", "--probe", dest="probe_file", type="str",
                      default="", help="Probe validation file")
    '''parser.add_option("-n", "--nooutput", dest="nooutput", action="store_true",
                      default=False, help="Disables file output, use with python -i <script>")
    parser.add_option("-s", "--ssllab", dest="ssllab", action="store_true",
                      default=False, help="Output to console SSL lab script for SSL cert and key object creation")
    parser.add_option("-t", "--notime", dest="timestamp", action="store_false",
                      default=True, help="Disables appending timestamp to the end of 'checking' output file")'''
    (options, args) = parser.parse_args()
    
    if options.ace_file and options.probe_file:
        ace = aceconf(options.ace_file)
        probes = probe_validation(options.probe_file)
        
    else:
        parser.print_help()