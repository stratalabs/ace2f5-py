#!/usr/bin/python
from optparse import OptionParser

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="file", type="str",
                      default="", help="ACE to F5 checking file")
    (options, args) = parser.parse_args()
    
    if options.file:
        writing_f5 = False
        checking = open(options.file, 'rt')
        checking_file = options.file.split('.')[0] + '.output'
        with open(checking_file, 'wt') as f:
            for line in checking:
                if line.startswith('######## ACE ########') or line.startswith('###### ERRORS #######'):
                    writing_f5 = False
                if writing_f5 and not line.startswith('#') and not line.startswith('\n'):
                    print(line, file=f, end="")
                if line.startswith('######## F5 ########'):
                    writing_f5 = True
                