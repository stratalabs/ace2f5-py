import ace2f5 as acefrom
import ace2f5 as aceto
import difflib
from optparse import OptionParser


if __name__ == '__main__':
    #parser = OptionParser('usage: %prog -f [ace from (old)] -t [ace to (new)]')
    parser = OptionParser()
    parser.add_option("-f", "--from", dest="filefrom", type="str", default="", help="ACE Configuration from (old)")
    parser.add_option("-t", "--to", dest="fileto", type="str", default="", help="ACE Configuration to (new)")
    (options, args) = parser.parse_args()

    if options.filefrom and options.fileto:
        conffrom = acefrom.aceconf(options.filefrom)
        confto = aceto.aceconf(options.fileto)
    else:
        print('Some Error')