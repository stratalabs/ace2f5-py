#!/usr/bin/python
from ace2f5 import *


def serverfarm_rserver_blocks(ServerfarmHost):
    blocks = []
    block = ''
    for line in ServerfarmHost.config.splitlines()[1:]:
        #print(line)
        if line.startswith('  rserver') and not block:
            block = line
            #print('new block', block)
        elif line.startswith('  rserver') and block:
            blocks.append(block)
            block = line
            #print('new block', block)
        else:
            block += line
    if block:
        blocks.append(block)
    for item in blocks:
        if item.strip().startswith('rserver') and item.split()[-1] != 'inservice':
            print('modify ltm pool %s members modify { %s:## { state user-down session user-disabled } }' % (ServerfarmHost.name, item.split()[1]))

if __name__=='__main__':
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="ace_file", type="str",
                      default="", help="ACE Configuration file")
    (options, args) = parser.parse_args()
    
    if options.ace_file:
        ace = aceconf(options.ace_file)
        for obj in ServerfarmHost.all_objs.values():
            serverfarm_rserver_blocks(obj)
    else:
        parser.print_help()