from optparse import OptionParser
import difflib
import re
import sys
import os
import time
from datetime import datetime, timezone

html_header = '''<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
    <meta http-equiv="Content-Type"
          content="text/html; charset=ISO-8859-1" />
    <title></title>
    <style type="text/css">
        table.diff {font-family:Courier; border:medium;}
        .diff_header {background-color:#e0e0e0}
        td.diff_header {text-align:right}
        .diff_next {background-color:#c0c0c0}
        .diff_add {background-color:#aaffaa}
        .diff_chg {background-color:#ffff77}
        .diff_sub {background-color:#ffaaaa}
    </style>
</head>
<body>
'''
html_tail = '''    <table class="diff" summary="Legends">
        <tr> <th colspan="2"> Legends </th> </tr>
        <tr> <td> <table border="" summary="Colors">
                      <tr><th> Colors </th> </tr>
                      <tr><td class="diff_add">&nbsp;Added&nbsp;</td></tr>
                      <tr><td class="diff_chg">Changed</td> </tr>
                      <tr><td class="diff_sub">Deleted</td> </tr>
                  </table></td>
             <td> <table border="" summary="Links">
                      <tr><th colspan="2"> Links </th> </tr>
                      <tr><td>(f)irst change</td> </tr>
                      <tr><td>(n)ext change</td> </tr>
                      <tr><td>(t)op</td> </tr>
                  </table></td> </tr>
    </table>
</body>
</html>
'''

parent_cmd = re.compile(r'^[a-z]|  class [A-Z]')

def unique_items(dictfrom, dictto):
    new_dic = {}
    for i in dictto.keys():
        if i not in dictfrom.keys():
            #print(dictto[i])
            #yield dictto[i]
            new_dic[i] = dictto[i]
    return new_dic

def changed_items(dictfrom, dictto, dictignor={}):
    changed_keys = []
    if dictignor:
        ignorkeys = list(dictignor.keys())
    else:
        ignorkeys = []
    for keyto in dictto.keys():
        if keyto not in ignorkeys and dictto[keyto] != dictfrom[keyto]:
            changed_keys.append(keyto)
        #else:
        #    print(keyto, 'in Unique item list!')
    return changed_keys

def filediff(text1, text2):
    m = SequenceMatcher(None, text1, text2)
    print(m.ratio())


def file_mtime(path):
    t = datetime.fromtimestamp(os.stat(path).st_mtime,
                               timezone.utc)
    return t.astimezone().isoformat()


class ConfBlocks:
    '''

    '''
    find_blocks = lambda self, str: [block for block in self.conf_blocks if block.startswith(str)]
    def _create_ace_objects(self, block):
        '''
        Takes a single block of Cisco ACE config and passes to correct class, each class is designed to have a .all_objs var
        which stores all the classes objects.
            i.e. object namespace is not required simply pass Cisco ACE config block to the class and the class will add to its
             own .all_objs list 
        '''
        if block.startswith('crypto chaingroup'):
            CryptoChaingroup(block)
        elif block.startswith('access-list'):
            ACL(block)
        elif block.startswith('interface vlan'):
            InterfaceVlan(block)
        elif block.startswith('probe dns'):
            ProbeDNS(block)
        elif block.startswith('probe tcp'):
            ProbeTCP(block)
        elif block.startswith('probe http'):
            ProbeHTTP(block)
        elif block.startswith('probe icmp'):
            ProbeICMP(block)
        elif block.startswith('probe smtp'):
            ProbeSMTP(block)
        elif block.startswith('parameter-map type ssl'):
            ParameterMapSSL(block)
        elif block.startswith('parameter-map type connection'):
            ParameterMapCon(block)
        elif block.startswith('action-list'):
            ActionList(block)
        elif block.startswith('rserver host'):
            RserverHost(block)
        elif block.startswith('rserver redirect'):
            RserverRedirect(block)
        elif block.startswith('ssl-proxy'):
            SSLProxy(block)
        elif block.startswith('serverfarm host'):
            ServerfarmHost(block)
        elif block.startswith('serverfarm redirect'):
            ServerfarmRedirect(block)
        elif block.startswith('sticky ip'):
            StickyIP(block)
        elif block.startswith('sticky http'):
            StickyHTTP(block)
        elif block.startswith('class-map match-any'):
            ClassMap(block)
        elif block.startswith('class-map match-all'):
            ClassMap(block)
        elif block.startswith('class-map tppe http'):
            ClassMap(block)
        elif block.startswith('policy-map type loadbalance'):
            PolicyLB(block)
        elif block.startswith('policy-map multi-match'):
            policy_map_multi = PolicyMapMultiMatch(block)
            class_block = ''
            for line in block.splitlines():
                    if line.startswith('  class'):
                        if class_block != '':
                            PolicyMapClass(class_block, policy_map_multi)
                        class_block = line + '\n'
                    elif line.startswith('    '):
                        class_block = class_block + line + '\n'
            if class_block != '':
                PolicyMapClass(class_block, policy_map_multi)
        elif block.startswith('ip route'):
            ipRoute(block)
        # If any blockes are not found they are added to the 'ace2f5.error_blocks' list
        else:
            self.error_blocks.append(block)

    def __init__(self, file):
        '''
        Take Cisco ACE configuration file and splits into list of configuration blocks.
        Pops each block for config block list and passes to self._create_ace_objects() for creation of ACE-to-F5 Objects
        '''
        self.conf_blocks = {}
        self.error_blocks = []
        
        # Clears block var ready for configuration split
        block = ''
        file = open(file, 'rt')
        # Take each line from file and split into blocks bases on the spacing at the start of each line.
        for line in file:
            # Regex match for line starting with [a-z], i.e. not a 'space', 'tab' or '#'
            name = ''
            if parent_cmd.match(line):
                # As new root configuration block found adds 'block' to list of blocks, clears the 'block' var and add first line
                if block != '':
                    name = block.strip().splitlines()[0]
                    self.conf_blocks[name] = block
                block = line
            # If line doesn't start with [a-z] line is part of block so adds the line to the 'block' var
            elif line.startswith('  '):
                block = block + line
        # Adds final block to list of blocks
        self.conf_blocks[name] = block

       # for block in self.conf_blocks:
        #    self._create_ace_objects(self.conf_blocks.pop(0))
        
        file.close()
        # Add ERROR message for checking file output
        self.error_blocks.append('\n### Conversion ERRORS ###\n')
    
    def __str__(self):
        return ' '.join(self.conf_blocks)

if __name__=='__main__':
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="file", type="str", default="", help="ACE Configuration one")
    parser.add_option("-d", "--diff", dest="diff", type="str", default="", help="ACE Configuration two")
    (options, args) = parser.parse_args()

    if options.file and options.diff:
        print(html_header)
        fromdate = file_mtime(options.file)
        todate = file_mtime(options.diff)
        #text1 = open(options.file).read()
        #text2 = open(options.diff).read()
        text1 = ConfBlocks(options.file)
        text2 = ConfBlocks(options.diff)

        m = difflib.SequenceMatcher(None, text1.conf_blocks.values(), text2.conf_blocks.values())
        p = round(1 - m.quick_ratio(),3) * 100
        print('%s%s Estimated changed.\n\n' % (str(p), '%'))
        #diff = difflib.HtmlDiff().make_file(text1.conf_blocks.values(), text2.conf_blocks.values(), options.file, options.diff, context=True, numlines=1)
        #diff = difflib.unified_diff(text1.conf_blocks.values(), text2.conf_blocks.values(), options.file, options.diff, fromdate, todate, n=1)
        #sys.stdout.writelines(diff)

        print('<p>##### Unique New configuration ####</p>')
        new_config = unique_items(text1.conf_blocks, text2.conf_blocks)
        new_keys_list = list(new_config.keys())
        new_config_str = '\n'.join(sorted(list(new_config.values())))
        print(difflib.HtmlDiff().make_table([], new_config_str.splitlines(), options.file, options.diff))

        print('<p>##### Unique Old configuration ####</p>')
        old_config = unique_items(text2.conf_blocks, text1.conf_blocks)
        old_keys_list = list(old_config.keys())
        old_config_str = '\n'.join(sorted(list(old_config.values())))
        print(difflib.HtmlDiff().make_table(old_config_str.splitlines(), [], options.file, options.diff))


        print('<p>##### Changed configuration ####</p>')
        changed_config = changed_items(text1.conf_blocks, text2.conf_blocks, new_config)
        print('number of changed config blocks %d' % len(changed_config))

        '''for key in sorted(changed_config):
            conf_from = text1.conf_blocks[key].splitlines()
            conf_to = text2.conf_blocks[key].splitlines()
            print(difflib.HtmlDiff().make_table(conf_from, conf_to, options.file, options.diff))
            print('<br>')'''
        conf_from = []
        conf_to = []
        for key in sorted(changed_config):
            conf_from += text1.conf_blocks[key].splitlines()
            conf_to += text2.conf_blocks[key].splitlines()
        print(difflib.HtmlDiff().make_table(conf_from, conf_to, options.file, options.diff))
        print('<br>')

        print(html_tail)
    else:
        print('Some Error')