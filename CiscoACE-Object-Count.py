# http://www.pennington.net/py/ciscoconfparse/api_CiscoConfParse.html
import os
import csv
from optparse import OptionParser
from ciscoconfparse import CiscoConfParse


windir = lambda str: '\\\\'.join(str.split('\\'))
getfilename = lambda str: '_'.join(str.split('\\')[-1].split())
stripstartswith = lambda str, regex: str.strip().startswith(regex)


def get_net_mask(str):
    if str == "255.255.255.255":
        return "32"
    if str == "255.255.255.254":
        return "31"
    if str == "255.255.255.252":
        return "30"
    if str == "255.255.255.248":
        return "29"
    if str == "255.255.255.240":
        return "28"
    if str == "255.255.255.224":
        return "27"
    if str == "255.255.255.192":
        return "26"
    if str == "255.255.255.128":
        return "25"
    if str == "255.255.255.0":
        return "24"
    if str == "255.255.254.0":
        return "23"
    if str == "255.255.252.0":
        return "22"
    if str == "255.255.248.0":
        return "21"
    if str == "255.255.240.0":
        return "20"
    if str == "255.255.224.0":
        return "19"
    if str == "255.255.192.0":
        return "18"
    if str == "255.255.128.0":
        return "17"
    if str == "255.255.0.0":
        return "16"
    if str == "255.254.0.0":
        return "15"
    if str == "255.252.0.0":
        return "14"
    if str == "255.248.0.0":
        return "13"
    if str == "255.240.0.0":
        return "12"
    if str == "255.224.0.0":
        return "11"
    if str == "255.192.0.0":
        return "10"
    if str == "255.128.0.0":
        return "9"
    if str == "255.0.0.0":
        return "8"
    if str == "0.0.0.0":
        return "0"


def routes(line, partition):
    #line = 'ip route 10.66.231.60 255.255.255.255 10.66.241.2'
    *_, network, subnet, gateway = line.split()
    subnet = get_net_mask(subnet)
    if line.startswith('ip route 0.0.0.0 0.0.0.0'):
        return 'net route /%s/default {\n    network default\n    gw %s\n}' % (partition, gateway)
    elif line.startswith('ip route'):
        return 'net route /%s/%s-%s {\n    network %s/%s\n    gw %s\n}' % (partition,
            network, subnet, network, subnet, gateway)
    elif line.startswith('ip management'):
        return 'sys management-route /Common/%s-%s {\n    network %s/%s\n    gateway %s\n}' % (network, 
            subnet, network, subnet, gateway)
    else:
        return '# ERROR: unknown line, %s' % (line)


def ssl_profile(block, partition):
    line1 = ''
    line_cert = ''
    line_chaingroup = ''
    line_key= ''
    line_defaults = '    defaults-from /Common/clientssl\n}'
    for line in block:
        if line.startswith('ssl-proxy'):
            #ssl-proxy service star.corp.intraxa
            #ltm profile client-ssl /part/star.corp.intraxa {
            *_, name = line.split()
            line1 = 'ltm profile client-ssl /%s\%s {\n' % (partition, name)
        elif line.strip().startswith('key'):
           * _, key = line.strip().split()
           key = key.split('.')[0]
           line_key = '    key /%s/%s.key\n' % (partition, key)
        elif line.strip().startswith('cert'):
            * _, cert = line.strip().split()
            cert = cert.split('.')[0]
            line_cert = '    cert /%s/%s.crt\n' % (partition, cert)
        elif line.strip().startswith('chaingroup'):
            * _, chaingroup = line.strip().split()
            chaingroup = chaingroup.split('.')[0]
            line_chaingroup = '    chain /%s/%s.crt\n' % (partition, chaingroup)
        elif line.strip().startswith('ssl advanced-options'):
            *_, advanced_options = line.strip().split()
            line_defaults = '    defaults-from /%s/%s\n}' % (partition, advanced_options)
        else:
            print('# ERROR: unknown line, %s' % (line))
    f5_block = '%s%s%s%s%s' % (line1, line_cert, line_chaingroup, line_key, line_defaults)
    return f5_block


'''
probe https SR-T1-DEFAULT-HTTPS
  description Default HTTPS Probe For SR-T1 Servers
  request method head url /healthcheck/health.html
  expect status 200 200
probe tcp SR-T1-DEV-CH-MQ-GATEWAY-1415
  description MQ Gateway DEV for external customer port 1415
  port 1415
probe tcp SR-T1-PRD-PROBE-AVR-1344
  description Specific Probe for Symantec AntiViRus servers running ICAP protocol
  port 1344
  interval 60
  faildetect 1
  passdetect interval 60
  passdetect count 1
  open 1
'''

def montior(block, partition):
    for line in block:
        if line.startswith('probe'):
            *_, probe_type, probe_name = line.split()
        elif stripstartswith(line, r'description'):
            _, *description = line.strip().split()
            description = '\"' + ' '.join(description) + "\""
        elif stripstartswith(line, r'request'):
            pass
        elif stripstartswith(line, r'expect'):
            pass
        elif stripstartswith(line, r'port'):
            pass
        elif stripstartswith(line, r'interval'):
            pass
        elif stripstartswith(line, r'faildetect'):
            pass
        elif stripstartswith(line, r'passdetect interval'):
            pass
        elif stripstartswith(line, r'passdetect count'):
            pass
        elif stripstartswith(line, r'open'):
            pass
        else:
            print('# ERROR: unknown line, %s' % (line))



def get_files_inc_subdir(dir=os.getcwd(), sub_count=5):
    files = []
    dirs = []
    for item in os.listdir(dir):
        if os.path.isfile(os.path.join(dir, item)):
            files.append(os.path.join(dir, item))
        elif os.path.isdir(os.path.join(dir, item)):
            dirs.append(os.path.join(dir, item))
    if dirs != [] and sub_count > 0:
        sub_count = sub_count - 1
        for directory in dirs:
            files = files + get_files_inc_subdir(directory)
    return files


def dedupe(items):
    seen = set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)


def acl_unique(acl_list):
    acl_names = []
    for item in acl_list:
        _, name, *_ = item.text.split()
        acl_names.append(name)
    return list(dedupe(acl_names))


def write_csv(file, header, rows):
    with open(file, 'w', newline='') as f:
        f_csv = csv.writer(f)
        f_csv.writerow(header)
        f_csv.writerows(rows)


def main_loop(file=""):
    basefile = os.path.basename(file)
    # Set object for ACE configuration from --file argument
    ace_conf = CiscoConfParse(file)
    # Find Parent e.g. crypto
    acl = ace_conf.find_objects(r'access-list')
    acl_2 = acl_unique(acl)
    probes = ace_conf.find_objects(r'^probe')
    parameter_maps = ace_conf.find_objects(r'^parameter-map')
    action_lists = ace_conf.find_objects(r'^action-list')
    rservers = ace_conf.find_objects(r'^rserver')
    
    serverfarms = ace_conf.find_objects(r'^serverfarm host')
    sf_rediret = ace_conf.find_objects(r'serverfarm redirect')
    stickies = ace_conf.find_objects(r'^sticky')
    class_maps = ace_conf.find_objects(r'^class-map match')
    class_maps_http = ace_conf.find_objects(r'^class-map type http')
    policy_map_lb = ace_conf.find_objects(r'^policy-map type loadbalance')
    vips = ace_conf.find_lines(r'virtual-address')
    mig_vips = ace_conf.find_lines(
        r'virtual-address 10.[1-2]00|virtual-address 192.168')
    vlans = ace_conf.find_lines(r'interface vlan')

    ip_routes = ace_conf.find_objects(r'ip route|ip management route')
    '''for ip_route in ip_routes:
        print(ip_route.text)
        print(routes(ip_route.text, basefile) + '\n')'''

    ssl_proxys = ace_conf.find_objects(r'^ssl-proxy')
    '''for ssl in ssl_proxys:
        print(ssl.text)
        print(ssl_profile(ace_conf.find_blocks(ssl.text), basefile))'''

    return [getfilename(file), len(vlans), len(acl), len(acl_2), len(probes), len(parameter_maps),
            len(action_lists), len(rservers), len(ssl_proxys), len(serverfarms),
            len(sf_rediret), len(stickies), len(class_maps), len(class_maps_http),
            len(policy_map_lb), len(vips)]


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="file", type="str",
                      default="", help="ACE Configuration File")
    parser.add_option("-o", "--output", dest="output", type="str",
                      default="output.conf", help="Output file")
    parser.add_option("-d", "--directory", dest="directory", type="str",
                      default="", help="Set search Directory")
    parser.add_option("-s", "--sub", dest="sub_dir", action="store_false",
                      default=True, help="Search single directory only specified with -d")
    (options, args) = parser.parse_args()

    #table_head = 'File acl acl_unique probes parameter_maps action_lists rservers ssl_proxys serverfarms redirects stickies class_maps class_map_http lb_policy vips vlans'
    header = ['File', 'vlans', 'acl', 'acl_unique', 'probes', 'parameter_maps', 'action_lists', 'rservers',
              'ssl_proxys', 'serverfarms', 'redirects', 'stickies', 'class_maps', 'class_map_http', 'lb_policy', 'vips']
    rows = []
    # Check see if file option
    if options.file:
        # print(table_head)
        rows.append(main_loop(options.file))
    elif options.directory != "" and options.file == "" and options.sub_dir == True:
        # print(table_head)
        files = get_files_inc_subdir(options.directory)
        for file in files:
            rows.append(main_loop(file))
    elif options.directory != "" and options.file == "" and options.sub_dir == False:
        # print(table_head)
        files = get_files_inc_subdir(options.directory, sub_count=0)
        for file in files:
            rows.append(main_loop(file))
    else:
        parser.print_help()

    write_csv(options.output, header, rows)