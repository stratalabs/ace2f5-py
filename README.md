# Cisco ACE to F5 Conversion - Python 3

## Synopsis

The goal of this script is to allow for automate migration from Cisco ACE to F5 LTM 11.x configuration. This is currently work in progress.

## Requirements

Python 3.3 or above is required to run.
Python can be downloaded from https://www.python.org/downloads/

## Usage

Generate the F5 configuration:
```
python ace2f5.py -f <ACE configuration input file> [-o <F5 configuration output file>] [-n NOT FULLY IMPLEMENTED (will disable out, i.e. validation to screen only]
```

If no output file is defined will output to ACE configuration file name plue '.checking'


Can also run and stay in Python CLI using the -i option e.g.

```
python -i ace2f5.py -f <ACE configuration input file>
```

After manually checking the output file run the following to generate a clean F5 TMOS configuration file with a `.output` extension.
This `.output` is the file to be imported into F5 LTM

```
python checking-output.py -f <ace2f5.py checking file>
```

### Manual checking

Output of the script will need to be checked and several F5 elements will need to be configured manually (to be automated at a later date),

0. ChangeGroups -> Certificate bundle with ChangeGroup name

0. parameter-map type ssl -> ltm client_ssl profile


# Fixes:
0. Sticky HTTP Header to be mapped as missing
0. Serverfarm loadbalancing method to be mapped.
0. connection limits under Rservers and Serverfarm, Rservers to be mapped
0. Virtual Server with port range e.g. `5 match virtual-address 1192.168.145.137 tcp range 5000 5100` not mapped correctly into F5
0. Backup Serverfarm and Serverfarm not merged into new single F5 pool
0. Action objects not migrated into F5 config e.g.
    ```
    action-list type modify http URL_RW
        ssl url rewrite location ".*"
    action-list type modify http http_to_https_header_rewrite
        header rewrite response Location header-value "http://(.*)" replace "https://%1"
    ```
0. Action objects not included as iRule in F5 VS
0. Add warning to check `connection advanced-options` under `polic-map, class`


# Features:
0. Output option to output TMSH script instead of F5 configuration file for merged (TO DO in seperate script)
0. Create `http` profile for each Virtual Server, i.e. do not use the default profile `http {}`

# Done:
0. Added logging to assist with ACE config validation, logging outputs all lines not mapped within each identified configuration block.
0. Multiple VIPs destiption line has duplicate description, e.g. `description description "Some info"`
0. Multiple VIPs during creation of multiple virtual servers add `/` on first line before `{` e.g. `ltm virtual WEB_VIP_02_1 \{`
0. HTTP Probe status code range correction e.g. `recv "HTTP/[0-2].[0-9] 200-202"` should be `recv "HTTP/[0-2].[0-9] 20[0-2]"`
0. ProbeHTTP `expect regex` line error
0. Serverfarm Rservers `Inservice` not checked
0. Policy-map multi-match not added to `checking` output file
0. VLAN not configured on F5 config based on `interface` `service-policy`, maps to `policy-map multi-match`

0. Output default LAB configuration setup, certificate setup using default.crt and default.key